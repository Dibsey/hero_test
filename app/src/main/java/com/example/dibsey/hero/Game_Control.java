package com.example.dibsey.hero;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Game_Control extends AppCompatActivity {


    private Handler mHandler = new Handler();

    private boolean vorZeitB = true;
    private boolean running = false;


    private int blockpoints = 3;
    private int health = 3;
    private int timerMax = 5;
    private int timerValue = timerMax;
    private int vorZeit = 5;

    ProgressBar TimerBar;

    Button Menu;
    Button Head;
    Button Torso;
    Button Legs;

    TextView TimerT;
    TextView vorCounter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game__control);


        TimerBar   = (ProgressBar)findViewById(R.id.timerBar);
        Menu       = (Button)findViewById(R.id.home);
        Head       = (Button)findViewById(R.id.head);
        Torso       = (Button)findViewById(R.id.torso);
        Legs       = (Button)findViewById(R.id.legs);
        TimerT     = (TextView)findViewById(R.id.timerT);
        vorCounter = (TextView)findViewById(R.id.vorZeitCounter);

        //Angriffsbuttons Unsichbar machen
        Head.setVisibility(View.INVISIBLE);
        Torso.setVisibility(View.INVISIBLE);
        Legs.setVisibility(View.INVISIBLE);

        vorCounter.bringToFront();

        TimerBar.setMax(timerMax);


        Menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Game_Control.this, Menue.class);
                startActivity(intent);
            }
        });


        startThread();

    }

    private void startThread(){

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {

                    //VorKampf Zeit wird herunter gezählt
                    while(vorZeitB){
                        vorCounter.setText("Fight starts\nin\n"+vorZeit);
                        if(vorZeit == 0) {
                            vorCounter.setVisibility(View.INVISIBLE);
                            //Startet den Timer für den Angriff
                            vorZeitB = false;
                            running = true;
                            vorZeit = 5;
                        }else{
                            vorZeit--;
                        }
                        //1000 für eine Sekunde
                        sleep(1000);
                    }


                    while(running) {
                        TimerBar.setProgress(timerValue);

                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                // Hier werden UI Elemente verändert
                                TimerT.setText(""+timerValue);
                                timerValue--;
                            }
                        });

                        //Thread wird ausgeschaltet wenn man stirbt
                        if(health == 0 || timerValue == 0){
                            running = false;
                            stopThread();
                        }
                        //1000 für eine Sekunde
                        sleep(1000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        };

        thread.start();
    }

    public void stopThread(){
        running = false;
        //Nachricht wenn man stirbt ( hier )
    }

    private void removeHP(){

    }
}
